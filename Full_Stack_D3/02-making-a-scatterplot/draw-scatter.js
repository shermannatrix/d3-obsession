async function drawScatter() {
	// Step 1: Access data
	let dataset = await d3.json("./../my_weather_data.json")
	// console.table(dataset[0]);

	const xAccessor = d => d.dewPoint
	const yAccessor = d => d.humidity
	const colorAccessor = d => d.cloudCover

	// Step 2: Create chart dimensions
	const width = d3.min([
		window.innerWidth * 0.9,
		window.innerHeight * 0.9
	])

	let dimensions = {
		width: width,
		height: width,
		margin: {
			top: 10,
			right: 10,
			bottom: 50,
			left: 50,
		},
	}

	// Having margins around the bounds allows us to alloate space
	// for our static chart elements (axes and legends).
	dimensions.boundedWidth = dimensions.width 
		- dimensions.margin.left
		- dimensions.margin.right
	
	dimensions.boundedHeight = dimensions.height
		- dimensions.margin.top
		- dimensions.margin.bottom

	// Step 3: Draw canvas
	const wrapper = d3.select("#wrapper")
		.append("svg")
		.attr("width", dimensions.width)
		.attr("height", dimensions.height)

	// think of "g" as a group element, and then we use transform 
	// to move it to the right and down.
	const bounds = wrapper.append("g")
		.style("transform", `translate(${
			dimensions.margin.left
		}px, ${
			dimensions.margin.top
		}px)`)

	// Step 4: Create scales
	const xScale = d3.scaleLinear()
		.domain(d3.extent(dataset, xAccessor))
		.range([0, dimensions.boundedWidth])
		.nice()
	
	const yScale = d3.scaleLinear()
		.domain(d3.extent(dataset, yAccessor))
		.range([dimensions.boundedHeight, 0])
		.nice()
	
	const colorScale = d3.scaleLinear()
		.domain(d3.extent(dataset, colorAccessor))
		.range(["skyblue", "darkslategrey"])
	
	// console.log(xScale.domain())
	// xScale.nice()
	// console.log(xScale.domain())

	// console.log(d3.extent(dataset, yAccessor))
	// console.log(yScale.domain())

	// Step 5: Draw data

	// const dots = bounds.selectAll("circle")
	// 	.data(dataset)
	// 	.enter().append("circle")
	// 	.attr("cx", d => xScale(xAccessor(d)))
	// 	.attr("cy", d => yScale(yAccessor(d)))
	// 	.attr("r", 5)
	// 	.attr("fill", "cornflowerblue");

	function drawDots(dataset) {
		const dots = bounds.selectAll("circle").data(dataset)

		dots.join("circle")	// since d3-seletion 1.4.0
			.attr("cx", d => xScale(xAccessor(d)))
			.attr("cy", d => yScale(yAccessor(d)))
			.attr("r", 5)
			.attr("fill", d => colorScale(colorAccessor(d)))
	
		// dots.enter()
		// 	.append("circle")
		// 	.merge(dots)	// combine current selection with another selection
		// 	.attr("cx", d => xScale(xAccessor(d)))
		// 	.attr("cy", d => yScale(yAccessor(d)))
		// 	.attr("r", 5)
		// 	.attr("fill", color)
	}
	
	//Demonstrate the data join
	//drawDots(dataset.slice(0, 200), "darkgrey")

	drawDots(dataset)

	// bounds.append("circle")
	// 	.attr("cx", dimensions.boundedWidth / 2)
	// 	.attr("cy", dimensions.boundedHeight / 2)
	// 	.attr("r", 5)
	// If this function is run twice, we'll end up drawing two sets
	// of dots.
	// dataset.forEach(d => {
	// 	bounds.append("circle")
	// 		.attr("cx", xScale(xAccessor(d)))
	// 		.attr("cy", yScale(yAccessor(d)))
	// 		.attr("r", 5)
	// })

	// Step 6: Draw peripherals

	const xAxisGenerator = d3.axisBottom()
		.scale(xScale)
	
	const xAxis = bounds.append("g")
		.call(xAxisGenerator)
		.style("transform", `translateY(${dimensions.boundedHeight}px)`)
	
	const xAxisLabel = xAxis.append("text")
		.attr("x", dimensions.boundedWidth / 2)
		.attr("y", dimensions.margin.bottom - 10)
		.attr("fill", "black")
		.style("font-size", "1.4em")
		.html("Dew point (&deg;F)")
	
	const yAxisGenerator = d3.axisLeft()
		.scale(yScale)
		.ticks(4)

	const yAxis = bounds.append("g")
		.call(yAxisGenerator)
	
	const yAxisLabel = yAxis.append("text")
		.attr("x", -dimensions.boundedHeight / 2)
		.attr("y", -dimensions.margin.left + 10)
		.attr("fill", "black")
		.style("font-size", "1.4em")
		.style("transform", "rotate(-90deg)")
		.style("text-anchor", "middle")
		.text("Relative Humidity")

	// Step 7: Set up interactions
}


drawScatter();
